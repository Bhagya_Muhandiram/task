const express = require("express");
const router = express.Router();
//Load controllers
const users=require("../../controllers/user.controller.js")


// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", users.register);

  // @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login",users.login);



module.exports = router;