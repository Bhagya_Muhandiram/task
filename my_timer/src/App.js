import React from 'react';
import logo from './logo.svg';
import './App.css';
import Timer from './components/timer';
import Home from './components/home'
import Register from './components/auth/Register'
import Login from "./components/auth/Login";
import  history  from './history';



import { BrowserRouter as Router,
  Switch,
  Route,
  Link} from "react-router-dom";

function App() {
  return (
    <Router history={history}>
<div className="App">
   
<Route exact path="/" component={Login} />
    <Route exact path="/register" component={Register} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/home" component={Home} />
  
</div>
    
    </Router>
  );
}

export default App;
