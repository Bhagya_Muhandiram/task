import React, { Component } from "react";
import { Link } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from "jwt-decode";
import UserDataService from "../../services/user.service";

class Login extends Component {
constructor(props){

    super(props);
}
componentDidMount(){

    // Remove token from local storage
  localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false

}

}

export default Login

