import React, { Component } from "react";
import { Link } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';


import UserDataService from "../../services/user.service"

class Register extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      errors: {}
    };
  }
onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

onSubmit = e => {
    e.preventDefault();

const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2
    };
console.log(newUser);

UserDataService.create(newUser,this.props.history).then(response=>{
  
  this.setState({
    name: response.data.name,
    email: response.data.email,
    password: response.data.password,
    password2:response.data.password2,
  });
  alert("Account Created !")
  this.props.history.push("/login")
}).catch(error=>{
  this.setState({
    errors:error.response.data
  })
  
});

  };

  componentDidMount(){
    console.log(this.props.location.state)
  }

render() {
    const { errors } = this.state;
return (
      <div className="container">
        <div style={{ marginTop: "4rem" }}  className="row">
          <div className="col s8 offset-s2">
            <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left"></i> Back to
              home
            </Link>
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <h3>
                <b>Register</b> below
              </h3>
              <p className="grey-text text-darken-1">
                Already have an account? <Link to="/login">Log in</Link>
              </p>
            </div>
            <div className='col 12' style={{paddingLeft:"40.250px" , paddingRight:"40.250px"}}>

            <Form>
            <Form.Group controlId="name" value={this.state.name}><span className="red-text">{errors.name}</span>
                <Form.Control size="sm" type="text" name='name' placeholder=" Name" onChange={this.onChange} error={errors.name}/>
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="email" value={this.state.email}><span className="red-text">{errors.email}</span>
                <Form.Control size="sm" type="email" name='email' placeholder="Email" onChange={this.onChange} error={errors.email} />
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password" value={this.state.password}><span className="red-text">{errors.password}</span>
                <Form.Control size="sm" type="password" name='password' placeholder="Password" onChange={this.onChange} error={errors.password}/>
            </Form.Group>

            <Form.Group controlId="password2" value={this.state.password2}><span className="red-text">{errors.password2}</span>
                <Form.Control size="sm" type="password" name='password2' placeholder="Confirm Password" onChange={this.onChange} error={errors.password2}/>
            </Form.Group>
  
  
  <Button variant="info" type="submit" className="btn btn-large waves-effect waves-light hoverable blue accent-3" onClick={this.onSubmit}> 
  Sign up
  </Button>
</Form>
            </div>
            
          </div>
        </div>
      </div>
    );
  }
}
export default Register;