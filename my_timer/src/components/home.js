import React from 'react';
import ms from 'pretty-ms';
import Timer from './timer';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import jwt_decode from "jwt-decode";




class Home extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      token:'',
      name:''
      
      
    }

  }

  componentDidMount(){
    const isuserAuthenticated = localStorage.getItem('jwtToken');
    
    const decoded = jwt_decode(isuserAuthenticated);
    this.setState({
      name:decoded.name
    })


console.log(decoded);

    if(!isuserAuthenticated){
    
      this.props.history.push("/login");
    }  
  }
 
  render() {
    
    return(
       <React.Fragment>
         <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" >
          <Navbar.Brand style={{fontFamily:'Trebuchet MS, sans-serif'}} href={'/home'}>My Timer</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">

              
            </Nav>
            <Nav>
    <h5 style={{color:'white'}}>Hello  {this.state.name} !</h5>
              
            </Nav>
          </Navbar.Collapse>
          </Navbar>
           <Timer/>


       </React.Fragment>

    )
  }
}
export default Home