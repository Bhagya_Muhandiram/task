import React from 'react';
import ms from 'pretty-ms';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'


class Timer extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      time2:0,
      time3:0,
      isOn: false,
      isOn2:false,
      isOn3:false,
      start2:0,
      start: 0,
      start3:0,
      
      
    }
    //timer 01
    this.startTimer1 = this.startTimer1.bind(this)
    this.stopTimer1 = this.stopTimer1.bind(this)
    this.resetTimer1 = this.resetTimer1.bind(this)

    //timer 02
    this.startTimer2 = this.startTimer2.bind(this)
    this.stopTimer2 = this.stopTimer2.bind(this)
    this.resetTimer2 = this.resetTimer2.bind(this)

    //timer 03
    this.startTimer3 = this.startTimer3.bind(this)
    this.stopTimer3 = this.stopTimer3.bind(this)
    this.resetTimer3 = this.resetTimer3.bind(this)
  }
  //timer 01 methods
  startTimer1() {
    this.setState({
      isOn: true,
      time: this.state.time,
      start: Date.now() - this.state.time
    })
    this.timer = setInterval(() => this.setState({
      time: Date.now() - this.state.start
    }), 10000);
  }
  stopTimer1() {
    this.setState({isOn: false})
    clearInterval(this.timer)
  }
  resetTimer1() {
    this.setState({time: 0, isOn: false})
  }
  //timer 02 methods
  startTimer2() {
    this.setState({
      isOn2: true,
      time2: this.state.time2,
      start2: Date.now() - this.state.time2
    })
    this.timer2 = setInterval(() => this.setState({
      time2: Date.now() - this.state.start2
    }), 1000);
  }
  stopTimer2() {
    this.setState({isOn2: false})
    clearInterval(this.timer2)
  }
  resetTimer2() {
    this.setState({time2: 0, isOn2: false})
  }
//timer 03 methods

startTimer3() {
    this.setState({
      isOn3: true,
      time3: this.state.time3,
      start3: Date.now() - this.state.time3
    })
    this.timer3 = setInterval(() => this.setState({
      time3: Date.now() - this.state.start3
    }), 100);
  }
  stopTimer3() {
    this.setState({isOn3: false})
    clearInterval(this.timer3)
  }
  resetTimer3() {
    this.setState({time3: 0, isOn3: false})
  }


  render() {
    let start = (this.state.time == 0) ?
    <Button variant="success" onClick={this.startTimer1}>Start</Button>:null
      
    let stop = (this.state.time == 0 || !this.state.isOn) ?
      null :
      <Button variant="danger" onClick={this.stopTimer1}>Stop</Button>
      
    let resume = (this.state.time == 0 || this.state.isOn) ?
      null :
      <Button variant="warning" onClick={this.startTimer1}>Resume</Button>
      
    let reset = (this.state.time == 0 || this.state.isOn) ?
      null :
      <Button variant="secondary" onClick={this.resetTimer1}>Reset</Button>

      //timer 02

      let start2 = (this.state.time2 == 0) ?
    <Button variant="success" onClick={this.startTimer2}>Start</Button>:null
      
    let stop2 = (this.state.time2 == 0 || !this.state.isOn2) ?
      null :
      <Button variant="danger" onClick={this.stopTimer2}>Stop</Button>
      
    let resume2 = (this.state.time2 == 0 || this.state.isOn2) ?
      null :
      <Button variant="warning" onClick={this.startTimer2}>Resume</Button>
      
    let reset2 = (this.state.time2 == 0 || this.state.isOn2) ?
      null :
      <Button variant="secondary" onClick={this.resetTimer2}>Reset</Button>

//timer 03
let start3 = (this.state.time3 == 0) ?
    <Button variant="success" onClick={this.startTimer3}>Start</Button>:null
      
    let stop3 = (this.state.time3 == 0 || !this.state.isOn3) ?
      null :
      <Button variant="danger" onClick={this.stopTimer3}>Stop</Button>
      
    let resume3 = (this.state.time3 == 0 || this.state.isOn3) ?
      null :
      <Button variant="warning" onClick={this.startTimer3}>Resume</Button>
      
    let reset3 = (this.state.time3 == 0 || this.state.isOn3) ?
      null :
      <Button variant="secondary" onClick={this.resetTimer3}>Reset</Button>
      
      
      
    return(
        <React.Fragment>
            <div style={{height:'100vh',width:'50%',float:'left',display:'flex',justifyContent:'center',alignItems:'center'}}>

            <Card  bg='success' style={{ width: '18rem' ,margin:'2%',backgroundColor:'A7FFEB'}} >
                <Card.Body>
                <Card.Title>Total Timer</Card.Title>
                    <h4 className="mb-2" style={{color:'white'}}>{ms(this.state.time+this.state.time2+this.state.time3,{colonNotation: true},{separateMilliseconds:true})}</h4>
                    
                    
                </Card.Body>
            </Card>
            </div>
            <div style={{height:'100vh',width:'50%',display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'column'}}>
            <Card bg='warning' style={{ width: '18rem' ,margin:'2%'}} >
                <Card.Body>
                <Card.Title>Timer 01</Card.Title>
                    <h4 className="mb-2" style={{color:'white'}}>{ms(this.state.time,{colonNotation: true},{separateMilliseconds:true})}</h4>
                    <Card.Text>
                    {start}
                    {resume}
                    {stop}
                    {reset}
                    </Card.Text>
                    
                </Card.Body>
            </Card>

            <Card Card bg='warning' style={{ width: '18rem' ,margin:'2%'}} >
                <Card.Body>
                <Card.Title>Timer 02</Card.Title>
                    <h4 className="mb-2" style={{color:'white'}}>{ms(this.state.time2,{colonNotation: true},{separateMilliseconds:true})}</h4>
                    <Card.Text>
                    {start2}
                    {resume2}
                    {stop2}
                    {reset2}
                    </Card.Text>
                    
                </Card.Body>
            </Card>
            
            <Card Card bg='warning' style={{ width: '18rem' ,margin:'2%'}} >
                <Card.Body>
                <Card.Title>Timer 03</Card.Title>
                    <h4 className="mb-2" style={{color:'white'}} >{ms(this.state.time3,{colonNotation: true},{separateMilliseconds:true})}</h4>
                    <Card.Text>
                    {start3}
                    {resume3}
                    {stop3}
                    {reset3}
                    </Card.Text>
                    
                </Card.Body>
            </Card>
            </div>

        </React.Fragment>

    )
    
  }
}
export default Timer