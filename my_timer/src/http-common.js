import axios from "axios";

export default axios.create({
  baseURL: "https://travel-lodge-backend.herokuapp.com/api/users",
  headers: {
    "Content-type":  "application/json",
    
  }
});