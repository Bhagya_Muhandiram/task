import http from "../http-common";

class UserDataService {
    //Register User
    create(data) {
        return http.post("/register", data);
      }
      
    //User Login
    login(data){
        return http.post("/login",data)
    }

    get(id) {
      return http.get(`/test/${id}`);
    }
  
    update(id, data) {
      return http.put(`/test/${id}`, data);
    }
  
    delete(id) {
      return http.delete(`/test/${id}`);
    }
  
    deleteAll() {
      return http.delete(`/test`);
    }
  
    
  }
  
  export default new UserDataService();