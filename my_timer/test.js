import React from 'react';
import ms from 'pretty-ms';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'


class Timer extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      isOn: false,
      start: 0,
      increment_time:0,
      timer_name:''
      
    }
    this.startTimer = this.startTimer.bind(this)
    this.stopTimer = this.stopTimer.bind(this)
    this.resetTimer = this.resetTimer.bind(this)
  }
  startTimer() {
    this.setState({
      isOn: true,
      time: this.state.time,
      start: Date.now() - this.state.time
    })
    this.timer = setInterval(() => this.setState({
      time: Date.now() - this.state.start
    }), this.props.increment_time);
  }
  stopTimer() {
    this.setState({isOn: false})
    clearInterval(this.timer)
  }
  resetTimer() {
    this.setState({time: 0, isOn: false})
  }
  render() {
    let start = (this.state.time == 0) ?
    <Button variant="success" onClick={this.startTimer}>Start</Button>:null
      
    let stop = (this.state.time == 0 || !this.state.isOn) ?
      null :
      <Button variant="danger" onClick={this.stopTimer}>Stop</Button>
      
    let resume = (this.state.time == 0 || this.state.isOn) ?
      null :
      <Button variant="warning" onClick={this.startTimer}>Resume</Button>
      
    let reset = (this.state.time == 0 || this.state.isOn) ?
      null :
      <Button variant="secondary" onClick={this.resetTimer}>Reset</Button>
      
    return(
        <React.Fragment>
            <Card style={{ width: '18rem' ,margin:'2%',backgroundColor:'#EDE7F6'}} >
                <Card.Body>
                    <Card.Title>{this.props.name}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{ms(this.state.time,{colonNotation: true},{separateMilliseconds:true})}</Card.Subtitle>
                    <Card.Text>
                    {start}
                    {resume}
                    {stop}
                    {reset}
                    </Card.Text>
                    
                </Card.Body>
            </Card>

            <Card style={{ width: '18rem' ,margin:'2%',backgroundColor:'#EDE7F6'}} >
                <Card.Body>
                    <Card.Title>{this.props.name}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">{ms(this.state.time,{colonNotation: true},{separateMilliseconds:true})}</Card.Subtitle>
                    <Card.Text>
                    {start}
                    {resume}
                    {stop}
                    {reset}
                    </Card.Text>
                    
                </Card.Body>
            </Card>

        </React.Fragment>
      

    )
  }
}
export default Timer